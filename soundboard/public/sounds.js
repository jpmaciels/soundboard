const Ai = require('./sounds/ai.mp3');
const Aiaiai = require('./sounds/aiaiai.mp3');
const Atumalaca = require('./sounds/atumalaca-risada.mp3');
const Baburinha = require('./sounds/baburinha.mp3');
const Brincadeira = require('./sounds/brincadeira.mp3');
const BaDunTss = require('./sounds/badumtss.mp3');
const BemTeVi = require('./sounds/Bem-te-vi.mp3');
const Brasilsil = require('./sounds/Brasil-sil-sil.mp3');
const BRUH = require('./sounds/BRUH.mp3');
const Cavalo = require('./sounds/cavalo.mp3');
const Chega = require('./sounds/chega.mp3');
const Dança = require('./sounds/dança.mp3');
const Demais = require('./sounds/demais.mp3');
const DiscordJoin = require('./sounds/discord-join.mp3');
const DiscordNotification = require('./sounds/discord-notification.mp3');
const dramatic = require('./sounds/dramaticChipmunkSFX.mp3');
const EleGosta = require('./sounds/elegosta.mp3');
const Errrrou = require('./sounds/Errou.mp3');
const Error = require('./sounds/erroWindows.mp3');
const EuNumEntendi = require('./sounds/eu-nao-entendi.mp3');
const EsqueçaTudo = require('./sounds/esqueçatudo.mp3');
const MarioCoin = require('./sounds/mario-coin.mp3');
const Metalgear = require('./sounds/metalgearsolid.swf.mp3');
const MinecraftHurt = require('./sounds/minecraft_hurt.mp3');
const Nao = require('./sounds/nao.mp3');
const NaoVaiNao = require('./sounds/ele-nao-vai-nao.mp3');
const Oooof = require('./sounds/Oof-roblox.mp3');
const Pare = require('./sounds/pare.mp3');
const BauDaCasaPropia = require('./sounds/piao-do-bau.mp3');
const QIsso = require('./sounds/qissomeufilho.mp3');
const Rapaz = require('./sounds/rapaz.mp3');
const Ratinho = require('./sounds/ratinho.mp3');
const SadOhhh = require('./sounds/Sad_Oooohh.mp3');
const SentaQueLaVem = require('./sounds/senta_que_la_vem_a_história.mp3');
const SusAmogus = require('./sounds/Sus-Amongus.mp3');
const Tome = require('./sounds/tome.mp3');
const Tudo = require('./sounds/tudo.mp3');
const Uepa = require('./sounds/uepa.mp3');
const Ui = require('./sounds/ui.mp3');
const VineBoom = require('./sounds/vine-boom.mp3');
const WOW = require('./sounds/WOW.mp3');

export {
    Ai,
    Aiaiai,
    Atumalaca,
    Baburinha,
    Brincadeira,
    BaDunTss,
    BauDaCasaPropia,
    BemTeVi,
    Brasilsil,
    BRUH,
    Cavalo,
    Chega,
    Dança,
    Demais,
    DiscordJoin,
    DiscordNotification,
    dramatic,
    EleGosta,
    Errrrou,
    Error,
    EuNumEntendi,
    EsqueçaTudo,
    MarioCoin,
    Metalgear,
    MinecraftHurt,
    Nao,
    NaoVaiNao,
    Oooof,
    Pare,
    QIsso,
    Rapaz,
    Ratinho,
    SadOhhh,
    SentaQueLaVem,
    SusAmogus,
    Tome,
    Tudo,
    Uepa,
    Ui,
    VineBoom,
    WOW
};
