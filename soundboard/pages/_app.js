import '../styles/globals.css'

function SoundBoardApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default SoundBoardApp
