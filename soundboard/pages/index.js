import Head from 'next/head'
import styles from '../styles/Home.module.css'
import useSound from 'use-sound';
import React from "react";
import { Slider } from '@mui/material';

import * as sounds from "../public/sounds";

const Index = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Soundboard</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          SOUNDBOARD LEGAL
        </h1>

        <Sounds></Sounds>

      </main>
    </div>
  );
}

const Sounds = () => {
  const [playbackRate, setPlaybackRate] = React.useState(1);

  const Button = ({ sound, title }) => {
    const [play] = useSound(sound, { playbackRate, interrupt: true });

    return <a className={styles.card} onClick={play}>
      {title}
    </a>;
  };

  return (
    <div className={styles.grid}>

      <Button sound={sounds.Ai} title="Ai"></Button>
      <Button sound={sounds.Aiaiai} title="Aiaiai"></Button>
      <Button sound={sounds.Atumalaca} title="Atumalaca Risada"></Button>
      <Button sound={sounds.Baburinha} title="Baburinha"></Button>
      <Button sound={sounds.Brincadeira} title="É Brincadeira"></Button>
      <Button sound={sounds.BaDunTss} title="BaDunTss"></Button>
      <Button sound={sounds.BauDaCasaPropia} title="PiãoBauDaCasaPropia"></Button>
      <Button sound={sounds.BemTeVi} title="BemTeVi"></Button>
      <Button sound={sounds.Brasilsil} title="Brasilsil"></Button>
      <Button sound={sounds.BRUH} title="BRUH"></Button>
      <Button sound={sounds.Cavalo} title="Cavalo"></Button>
      <Button sound={sounds.Chega} title="Chegaaaa"></Button>
      <Button sound={sounds.Demais} title="Demais"></Button>
      <Button sound={sounds.DiscordJoin} title="Discord Join"></Button>
      <Button sound={sounds.DiscordNotification} title="Discord Notification"></Button>
      <Button sound={sounds.dramatic} title="dramatic"></Button>
      <Button sound={sounds.Errrrou} title="Errrrou"></Button>
      <Button sound={sounds.Error} title="Error"></Button>
      <Button sound={sounds.EsqueçaTudo} title="Esqueça Tudo"></Button>
      <Button sound={sounds.EuNumEntendi} title="Eu num entendi o que ele falou"></Button>
      <Button sound={sounds.MarioCoin} title="Mario Coin"></Button>
      <Button sound={sounds.Metalgear} title="Metalgear"></Button>
      <Button sound={sounds.MinecraftHurt} title="Minecraft Hurt"></Button>
      <Button sound={sounds.Nao} title="Não."></Button>
      <Button sound={sounds.NaoVaiNao} title="Não vai não"></Button>
      <Button sound={sounds.Oooof} title="Oooof"></Button>
      <Button sound={sounds.Pare} title="Pare"></Button>
      <Button sound={sounds.QIsso} title="Que isso meu filho, calma"></Button>
      <Button sound={sounds.Rapaz} title="Rapaz"></Button>
      <Button sound={sounds.SadOhhh} title="ohhhh sad"></Button>
      <Button sound={sounds.SentaQueLaVem} title="Senta Que La Vem História"></Button>
      <Button sound={sounds.SusAmogus} title="SusAmogus"></Button>
      <Button sound={sounds.Tome} title="Tome"></Button>
      <Button sound={sounds.Uepa} title="Uepa"></Button>
      <Button sound={sounds.Ui} title="Ui"></Button>
      <Button sound={sounds.VineBoom} title="VineBoom"></Button>
      <Button sound={sounds.WOW} title="WOW"></Button>


      <Slider
        value={playbackRate}
        min={0.3}
        step={0.05}
        max={2}
        onChange={(e, newValue) => setPlaybackRate(newValue)}
        valueLabelDisplay="auto"
      />
    </div>
  );
}

export default Index;
